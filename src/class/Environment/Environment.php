<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Environment;

use Nora\Core\Component\Componentable;

/**
 * 設定構成要素
 */
class Environment
{
    use Componentable;

    public function afterSetScope( )
    {
        $this->setup();
    }


    public function setup( )
    {
        if ($this->hasComponent('Configure'))
        {
            $config = $this->getComponent('Configure');
            if ($config->has('display_errors'))
            {
                ini_set('display_errors', $config->read('display_errors'));
            }
            if ($config->has('error_reporting'))
            {
                ini_set('error_reporting', $config->read('error_reporting'));
            }
            if ($config->has('mbstring.language'))
            {
                mb_language($config->read('mbstring.language'));
            }
            if ($config->has('mbstring.internal_encoding'))
            {
                mb_internal_encoding($config->read('mbstring.internal_encoding'));
            }
        }
    }

    /**
     * 登録
     */
    public function register ( )
    {
        // ハンドラ系の登録
        set_error_handler([$this, 'phpErrorHandler']);
        set_exception_handler([$this, 'phpExceptionHandler']);
        register_shutdown_function([$this, 'phpShutdownHandler']);
        return $this;
    }

    /**
     *  int $errno , string $errstr [, string $errfile [, int $errline [, array $errcontext ]
     */
    public function phpErrorHandler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        $this->fire('php.error', [
            'errno'      => $errno,
            'errstr'     => $errstr,
            'errfile'    => $errfile,
            'errline'    => $errline,
            'errcontext' => $errcontext
        ]);
    }

    /**
     */
    public function phpExceptionHandler($exception)
    {
        $this->fire('php.exception', [
            'exception' => $exception
        ]);
    }

    /**
     */
    public function phpShutdownHandler( )
    {
        $error = error_get_last();
        if ($error['type'] > 0) {
            $this->phpErrorHandler(
                $error['type'],
                $error['message'],
                $error['file'],
                $error['line'],
                []
            );
        }
        $this->fire('php.shutdown');
    }


}

