<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Logging;

use Nora\Core\Util\Tagging\TaggingAccess;


/**
 * ログオブジェクト
 */
class Log
{
    use TaggingAccess;

    /**
     * ログメッセージ
     * @var array
     */
    private $_msg;

    /**
     * ログレベル
     * 
     * @see Nora\Log\LogLevel
     * @var array
     */
    private $_level;

    /**
     * ログタイム
     * 
     * @var int
     */
    private $_time;

    /**
     * トレースオフセット
     * 
     * @var int
     */
    private $_trace_offset;

    /**
     * 新しいログを作成
     *
     * @param int $level
     * @param string $msg
     * @param string $tag
     * @param array $args
     * @return Log
     */
    static public function create ($level, $msg, $tag, $trace = false)
    {
        $log = new Log();
        $log->_level = is_string($level) ? LogLevel::toInt($level): $level;
        $log->_msg   = $msg;
        $log->addTag($tag);
        $log->_time  = time();
        $log->_trace = ($trace === true) ? $log->trace(): null;
        return $log;
    }

    /**
     * トレース
     */
    public function trace( )
    {
        ob_start( );
        debug_print_backtrace( );
        $trace = ob_get_clean();
        return $trace;
    }

    /**
     * ログメッセージを配列で取得
     */
    public function getFields( )
    {
        if (is_string($this->_msg))
        {
            return [
                'message' => $this->_msg
            ];
        }

        if (is_object($this->_msg))
        {
            return [
                get_class($this->_msg) => var_export($this->_msg, true)
            ];
        }
        return $this->_msg;
    }

    public function getFieldsText( )
    {
        $fields = $this->getFields();

        $txt = "";
        array_walk($fields, function ($v, $k) use (&$txt) {
            $txt.= "$k:$v, ";
        });
        return  substr($txt, 0, strlen($txt) - 2);
    }

    /**
     * レベルをテキストで取得
     */
    public function getLevelText( )
    {
        return LogLevel::toString($this->getLevel());
    }

    /**
     * レベルをで取得
     */
    public function getLevel( )
    {
        return $this->_level;
    }

    /**
     * Jsonでプリントする
     */
    public function toPrettyJson( )
    {
        return $this->toJson(JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    /**
     * タグ操作
     */
    public function tag( )
    {
        return $this->_tag;
    }

    /**
     * タグをテキストで取得
     */
    public function getTagText( )
    {
        return $this->getTagStorage()->toString(',');
    }


    /**
     * タグマッチ
     */
    public function tagMatch($tag, $hit = null)
    {
        return $this->searchTag($tag, $this);
    }

    /**
     *
     */
    public function getTime($format = 'Y-m-d H:i:s')
    {
        return date($format, $this->_time);
    }

    /**
     * Json化
     */
    public function toJson($flag = 0)
    {
        return json_encode([
            '@time' => date('Y-m-d H:i:s', $this->_time),
            '@level' => $this->getLevelText(),
            '@fields' => $this->getFields(),
            '@tag' => $this->getTagStorage()->toArray()
        ], $flag);
    }
}
