<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Component;

use Nora\Core\Logging\LoggerAccess;
use Nora\Core\Logging\Log;
use function Nora\__;

/**
 * 機能の最小単位
 */
trait Componentable
{
    /**
     * コンポーネントからログが吐き出せる
     * ようにする
     */
    use LoggerAccess;

    /**
     * スコープが使えるようにする
     */
    use ScopeAccess;


    /**
     * ログ検出時
     */
    protected function log(Log $log)
    {
        $log->addTag(
            strtolower(
                str_replace('\\', '.', get_class($this))));

        $this->getScope()->fire('log', [
            'log' => $log
        ]);
    }

    /**
     * スコープ登録時の処理
     */
    protected function afterSetScope( )
    {
        $this->initComponent();
    }

    protected function initComponent( )
    {
        $this->getScope()->addTag(get_class($this));
        $this->getScope()->setComponent([
            'self' => $this
        ]);
        foreach(get_class_methods($this) as $m)
        {
            if (0 === strncmp($m, 'boot', 4))
            {
                $this->setComponent(
                    substr($m, 4), [$this, $m]
                );
            }
        }

        $this->initComponentImpl();
    }

    abstract protected function initComponentImpl();

    /**
     * ステータスを取得する
     */
    public function status ( )
    {
        return [
            'scope' => $this->getScope()->status(),
        ];
    }

    /**
     * Inject実行する
     */
    public function inject($spec, $args = [], $over = [])
    {
        return $this->getScope()->inject($spec, $args, $over);
    }

    /**
     * マジックメソッド
     */
    public function __call($name, $args)
    {
        // 処理をスコープへ委ねる
        if ($this->isCallable($name, false))
        {
            return $this->callArray($name, $args, false);
        }

        $this->err(
            sprintf(__('%sは定義されていません'), $name)
        );
    }

    public function isCallable ($name, $deep = true)
    {
        if(true === $this->getScope()->isCallable($name, $deep))
        {
            return true;
        }
        return method_exists($this, $name);
    }

    public function callArray ($name, $args, $deep = true)
    {
        if(true === $this->getScope()->isCallable($name, $deep))
        {
            return $this->getScope()->callArray($name, $args, $deep);
        }
        return call_user_func_array([$this, $name], $args);
    }
}
