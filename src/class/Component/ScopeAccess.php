<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Component;

use Nora\Core\Scope\Scope;

/**
 * スコープを使用する
 */
trait ScopeAccess
{
    /**
     * スコープ
     */
    private $_scope;

    /**
     * スコープをセットする
     */
    public function setScope($scope)
    {
        $this->_scope = $scope;

        // スコープがセットされたことを通知する
        $this->afterSetScope($scope);

        return $this;
    }

    /**
     * スコープセット後に呼ばれるメソッド
     */
    abstract protected function afterSetScope( );

    /**
     * スコープを取得する
     *
     * 設定されていなければ新しく作成する
     */
    public function getScope( )
    {
        if ($this->_scope instanceof Scope)
        {
            return $this->_scope;
        }

        return $this->setScope(new Scope())->getScope();
    }
}
