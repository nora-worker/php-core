<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Component;

use Nora\Core\Logging\LoggerAccess;
use Nora\Core\Logging\Log;
use function Nora\__;

/**
 * 機能の最小単位
 */
class Component 
{
    use Componentable;

    protected function initComponentImpl( )
    {
    }
}
