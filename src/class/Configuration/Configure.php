<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Configuration;

use Nora\Core\Component\Componentable;
use Nora\Core\Exception\ApplicationError;
use function Nora\__;

/**
 * 設定構成
 */
class Configure extends Item
{
    use Componentable;

    protected function initComponentImpl ( )
    {
    }

    public function load($dir, $name)
    {
        if (func_num_args() > 2)
        {
            $args = func_get_args( );

            for($i=1; $i<count($args); $i++)
            {
                $this->load($dir, $args[$i]);
            }
            return $this;
        }

        if (is_array($dir))
        {
            foreach($dir as $d)
            {
                $this->load($d, $name);
            }
            return $this;
        }


        $file = $dir.'/'.$name.'.php';

        if (file_exists($file)) {
            $this->loadFile($file);
        }

        $files = $dir.'/'.$name.'/*.php';

        foreach(glob($files) as $file)
        {
            $name = basename($file);
            $name = substr($name, 0, strpos($name, '.'));
            $this->loadFile($file, $name);
        }
    }

    private function loadFile($file, $name = null)
    {
        if (!file_exists($file))
        {
            throw new ApplicationError(
                sprintf(
                    __('%sファイルは存在しません'),
                    $file
                )
            );
        }

        $array = include $file;

        if ($name !== null) {
            $this->write($name, $array);
        }
        else{
            $this->write($array);
        }
    }

}
