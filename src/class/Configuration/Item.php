<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Configuration;

use Nora\Core\Component\Component;

/**
 * 設定構成要素
 */
class Item implements \IteratorAggregate
{
    const SEP = '.';

    private $_items = [];

    /**
     * 設定を追記
     *
     * @param string $name
     * @param mixed $value
     */
    public function append($name, $value = null)
    {
        $data = $this->readArray($name);
        $data[] = $value;
        $this->write($name, $data);
        return $this;
    }

    public function readArray($name)
    {
        $data = $this->read($name, []);
        if ($data instanceof Item){
            $data = $data->toArray();
        }else{
            $data = [$data];
        }
        return $data;
    }

    /**
     * 設定を書き込む
     *
     * @param string $name
     * @param mixed $value
     */
    public function write($name, $value = null)
    {
        if (is_array($name))
        {
            foreach($name as $k=>$v) $this->write($k, $v);
            return $this;
        }
        // セクションの有無を判定
        if (false !== $pos = strpos($name, self::SEP))
        {
            $section = substr($name, 0, $pos);

            // セクションがなければ作成する
            if (!$this->hasItem($section))
            {
                $this->createItem($section);
            }

            $this->getItem($section)->write(substr($name, $pos+1), $value);
            return $this;
        }

        $this->createItem($name, $value);
        return $this;
    }

    /**
     * 設定を読む
     *
     * @param string $name
     */
    public function read($name, $default = null)
    {

        if (false !== $pos = strpos($name, self::SEP))
        {
            $section = substr($name, 0, $pos);
            if (!$this->hasItem($section))
            {
                return $default;
            }
            $next = $this->getItem($section);
            return $next instanceof Item ? $next->read(substr($name, $pos+1), $default): $default;
        }

        if ($this->hasItem($name))
        {
            return $this->getItem($name);
        }
        return $default;
    }

    /**
     * 設定があるか
     */
    public function has($name)
    {
        if (false !== $pos = strpos($name, self::SEP))
        {
            $section = substr($name, 0, $pos);
            if (!$this->hasItem($section))
            {
                return false;
            }
            return $this->getItem($section)->has(substr($name, $pos+1));
        }

        return $this->hasItem($name);
    }

    protected function hasItem($name)
    {
        if (isset($this->_items[$name]))
        {
            return true;
        }
        return false;
    }

    protected function createItem($name, $value = [])
    {
        if (is_array($value))
        {
            $item = new Item();

            foreach($value as $k=>$v)
            {
                $item->write($k, $v);
            }
            $this->_items[$name] = $item;
            return $this;
        }

        $this->_items[$name] = $value;
    }

    protected function getItem($name)
    {
        return $this->_items[$name];
    }

    public function toArray( )
    {
        $array = [];
        foreach($this->_items as $k=>$v)
        {
            $array[$k] = $v instanceof Item ? $v->toArray(): $v;
        }
        return $array;
    }

    public function getIterator( )
    {
        foreach($this->_items as $k=>$v)
        {
            yield $k=>$v;
        }
    }
}

