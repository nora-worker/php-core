<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Util\Tagging\TaggingAccess;
use Nora\Core\Logging\Log;
use Nora\Core\Exception\ApplicationError;
use function Nora\__ as __;

/**
 * スコープオブジェクト
 *
 * - ヘルパのセット
 * - コンポーネントのセット
 * - またそれらのマージ
 */
class Scope
{
    /**
     * Scopeを階層構造にする
     */
    use TreeAccess;

    /**
     * EventSubject
     */
    use EventSubjectAccess;

    /**
     * タグ付けを可能にする
     */
    use TaggingAccess;

    private $_parent = false;
    private $_registry = [];
    private $_name;

    public function __construct ($name = 'root')
    {
        $this->_name = $name;
        $this->_parent = false;
        $this->setComponent('scope', $this);
    }

    public function __set($name, $value)
    {
        $this->_set($name, $value);
    }
    public function __get($name)
    {
        return $this->_get($name);
    }

    public function __unset($name)
    {
        unset($this->_registry[$name]);
    }

    public function __isset($name)
    {
        return isset($this->_registry[$name]);
    }


    /**
     * 名前を取得する
     */
    public function getName( )
    {
        if ($this->hasParent())
        {
            return $this->getParent()->getName().".".$this->_name;
        }
        return $this->_name;
    }


    /**
     * ログを受け取る
     */
    public function log(Log $log)
    {
        $this->fire('log', [
            'log' => $log
        ]);
    }

    /**
     * ヘルパマネージャーを取得
     */
    public function getHelperManager( )
    {
        if ($this->_has('HelperManager'))
        {
            return $this->_get('HelperManager');
        }
        $this->_set('HelperManager', new HelperManager($this));
        return $this->getHelperManager();
    }

    /**
     * ヘルパがあるか
     *
     * @param string $name
     * @param bool $deep 親を辿る
     */
    protected function hasHelper($name, $deep = true)
    {
        if ($this->getHelperManager()->hasHelper($name))
        {
            return true;
        }
        if($deep === true && $this->hasParent() === true)
        {
            return $this->getParent()->hasHelper($name, $deep);
        }

        if ($name{0} !== '@') return $this->hasHelper('@'.$name, true);
        return false;
    }

    /**
     * ヘルパを取得
     *
     * @param string $name
     * @param bool $deep 親を辿る
     */
    protected function getHelper($name, $deep = true)
    {
        if($this->getHelperManager()->hasHelper($name))
        {
            return $this->getHelperManager()->getHelper($name);
        }

        if($deep === true && $this->hasParent() === true)
        {
            return $this->getParent()->getHelper($name, $deep);
        }

        if ($name{0} !== '@') return $this->getHelper('@'.$name, true);

        throw new ApplicationError(sprintf(__('%sは登録されていません'), $name));
    }

    /**
     * ヘルパを登録
     *
     * @param string|array $name
     * @param mixed $spec
     */
    public function setHelper($name, $spec = null)
    {
        if ($spec === null && is_array($name))
        {
            foreach($name as $k=>$v)
            {
                $this->setHelper($k, $v);
            }
            return $this;
        }

        $this->getHelperManager()->setHelper($name, $spec);
        return $this;
    }


    /**
     * コンポーネントマネージャーを取得
     */
    public function getComponentManager( )
    {
        if ($this->_has('ComponentManager'))
        {
            return $this->_get('ComponentManager');
        }
        $this->_set('ComponentManager', new ComponentManager($this));
        return $this->getComponentManager();
    }

    /**
     * コンポーネントがあるか
     *
     * @param string $name
     * @param bool $deep 親を辿る
     */
    protected function hasComponent($name, $deep = true)
    {
        if($this->getComponentManager ()->hasComponent($name))
        {
            return true;
        }

        if ($deep === true && $this->hasParent() === true)
        {
            return $this->getParent()->hasComponent($name, $deep);
        }

        if ($name{0} !== '@') return $this->hasComponent('@'.$name, true);

        return false;
    }

    /**
     * コンポーネントを取得
     *
     * @param string $name
     * @param bool $deep 親を辿る
     */
    protected function getComponent($name, $deep = true)
    {
        if($this->getComponentManager()->hasComponent($name))
        {
            return $this->getComponentManager()->getComponent($name);
        }

        if ($deep === true && $this->hasParent() === true)
        {
            return $this->getParent()->getComponent($name, $deep);
        }

        if ($name{0} !== '@') return $this->getComponent('@'.$name, true);

        throw new ApplicationError(sprintf(__('%sは登録されていません'), $name));
    }

    /**
     * コンポーネントを登録
     *
     * @param string|array $name
     * @param mixed $spec
     */
    public function setComponent($name, $spec = null)
    {
        if ($spec === null && is_array($name))
        {
            foreach($name as $k=>$v)
            {
                $this->setComponent($k, $v);
            }
            return $this;
        }

        $this->getComponentManager()->setComponent(
            $name,
            $this->makeClosure($spec)
        );
        return $this;
    }

    /**
     * クロージャを作成する
     */
    public function makeClosure($spec, $over = [])
    {
        if ($spec instanceof Closure || is_callable($spec) || is_object($spec))
        {
            return $spec;
        }

        return function( ) use ($spec, $over) {
            $func = array_pop($spec);
            $injections = array_reverse($spec);
            $args = func_get_args();
            foreach($injections as $k)
            {
                array_unshift(
                    $args,
                    (isset($over[$k]) ? $over[$k]: $this->getComponent($k, true))
                );
            }
            return call_user_func_array(
                $func, $args
            );
        };
    }

    /**
     * Inject実行する
     */
    public function inject($spec, $args = [], $over = [])
    {
        $over['scope'] = $this;
        return call_user_func_array(
            $this->makeClosure($spec, $over), $args);
    }

    /**
     * レジストリから取得
     *
     * @param string $name
     */
    private function _get($name)
    {
        if (!isset($this->_registry[$name]))
        {
            throw new ApplicationError(sprintf(
                __("%sは存在しません"), $name
            ));
        }
        return $this->_registry[$name];
    }

    /**
     * レジストリにセット
     *
     * @param string $name
     * @param mixed $value
     */
    private function _set($name, $value)
    {
        return $this->_registry[$name] = $value;
    }

    /**
     * レジストリにあるか
     *
     * @param string $name
     * @return bool
     */
    private function _has($name)
    {
        return isset($this->_registry[$name]);
    }

    /**
     * ステータスを取得
     */
    public function status ( )
    {
        return [
            'name' => $this->getName(),
            'tags' => $this->getTagStorage()->toArray()
        ];
    }

    /**
     * スコープの呼び出し
     */
    public function __call ($name, $args)
    {
        if ($this->isCallable($name, false))
        {
            return $this->callArray($name, $args, false);
        }

        throw new ApplicationError(sprintf(
            __('%sは存在しないメソッドです'), $name
        ));
    }

    /**
     * コール
     */
    public function call($name, ...$args)
    {
        if ($this->isCallable($name, true))
        {
            return $this->callArray($name, $args, true);
        }
        throw new ApplicationError(sprintf(
            __('%sは存在しないメソッドです'), $name
        ));
    }

    /**
     * スクリプトの実行
     */
    public function runScript($file)
    {
        $spec = include $file;
        return $this->inject($spec);
    }

    /**
     * スコープコール
     */
    public function callArray ($name, $args, $deep = true)
    {
        if (method_exists($this, $name)) return call_user_func_array([$this, $name], $args);

        if ($this->hasHelper($name, $deep))
        {
            return $this->inject($this->getHelper($name, $deep),$args);
        }

        if ($this->hasComponent($name, $deep))
        {
            return $this->getComponent($name, $deep);
        }

        // _区切りの解決
        if (false !== $pos = strpos($name, '_'))
        {
            if ($this->hasComponent(substr($name, 0, $pos)))
            {
                $cmp = $this->getComponent(substr($name, 0, $pos));
                return call_user_func_array([$cmp, substr($name, $pos+1)], $args);
            }

            if ($this->hasComponent('ModuleLoader'))
            {
                if ($this->getComponent('ModuleLoader')->hasModule(substr($name, 0, $pos)))
                {
                    $module = $this->getComponent('ModuleLoader')->load(substr($name, 0, $pos));
                    $method = substr($name, $pos+1);
                    return call_user_func_array([$module, substr($name, $pos+1)], $args);
                }
            }
        }

        throw new ApplicationError(sprintf(
            __('%sは存在しないメソッドです'), $name
        ));
    }

    /**
     * スコープコール可能化
     */
    public function isCallable ($name, $deep = true)
    {
        if (method_exists($this, $name) && $name{0} !== '_') return true;

        if ($this->hasHelper($name, $deep))
        {
            return true;
        }

        if ($this->hasComponent($name, $deep))
        {
            return true;
        }


        // _区切りの解決
        if (false !== $pos = strpos($name, '_'))
        {
            if ($this->hasComponent(substr($name, 0, $pos), true))
            {
                $cmp = $this->getComponent(substr($name, 0, $pos), true);
                $method = substr($name, $pos+1);
                return method_exists($cmp, 'isCallable') ?
                    $cmp->isCallable($method, false):
                    method_exists($cmp, $method);
            }

            if ($this->hasComponent('ModuleLoader'))
            {
                if ($this->getComponent('ModuleLoader')->hasModule(substr($name, 0, $pos)))
                {
                    $module = $this->getComponent('ModuleLoader')->load(substr($name, 0, $pos));
                    $method = substr($name, $pos+1);
                    return method_exists($module, 'isCallable') ?
                        $module->isCallable($method, false):
                        method_exists($module, $method);
                }
            }
        }

        return false;
    
    }
}
