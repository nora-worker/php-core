<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Util\TagStorage;
use Nora\Core\Logging\Log;
use Nora\Core\Exception\ApplicationError;
use function Nora\__ as __;

/**
 * スコープオブジェクト
 *
 * - ヘルパのセット
 * - コンポーネントのセット
 * - またそれらのマージ
 */
trait TreeAccess
{
    /**
     * 親スコープをセットする
     *
     * @param Scope $scoep
     */
    public function setParent(Scope $scope)
    {
        $this->_parent = $scope;

        $this->setComponent('parent', $scope);

    }

    /**
     * 親スコープがあるか
     *
     * @return bool
     */
    public function hasParent( )
    {
        return !!$this->_parent;
    }

    /**
     * 親スコープを取得
     */
    public function getParent( )
    {
        if (!$this->hasParent())
        {
            throw new ApplicationError(__('親スコープはありません'));
        }
        return $this->_parent;
    }

    /**
     * 新しいスコープを作成
     */
    public function newScope($name = 'child')
    {
        $scope = new Scope($name);
        $scope->setParent($this);
        return $scope;
    }

    /**
     * ルートスコープを取得
     */
    public function rootScope( )
    {
        return $this->hasParent() === true ?
            $this->getParent()->rootScope():
            $this;
    }
}

