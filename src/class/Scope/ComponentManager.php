<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Util\TagStorage;
use Nora\Core\Logging\Log;
use Nora\Core\Exception\ApplicationError;
use function Nora\__ as __;

/**
 * コンポーネントマネージャー
 */
class ComponentManager
{
    private $_scope;
    private $_components   = [];
    private $_constructors = [];

    public function __construct (Scope $scope)
    {
        $this->_scope;
    }

    public function hasComponent($name)
    {
        $name = strtolower($name);

        if( isset($this->_components[$name]) || isset($this->_constructors[$name]))
        {
            return true;
        }
        return false;
    }

    public function getComponent($name)
    {
        $name = strtolower($name);

        if (isset($this->_components[$name]))
        {
            return $this->_components[$name];
        }
        elseif (isset($this->_constructors[$name]))
        {
            return $this->_components[$name] = call_user_func(
                $this->_constructors[$name]
            );
        }

        throw new ApplicationError(
            sprintf(__('%sは定義されていません'), $name),
            $this
        );
    }

    public function setComponent($name, $spec)
    {
        $name = strtolower($name);
        if (is_object($spec) && !is_callable($spec))
        {
            $this->_components[$name] = $spec;
            return $this;
        }

        $this->_constructors[$name] = $spec;
        return $this;
    }
}

