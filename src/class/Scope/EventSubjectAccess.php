<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Event;

use function Nora\__ as __;

/**
 * イベントサブジェクト
 */
trait EventSubjectAccess
{
    use Event\EventSubjectAccess {
        Event\EventSubjectAccess::makeObserver as private parentMakeObserver;
        Event\EventSubjectAccess::fire as private parentFire;
    }

    protected function makeObserver($spec)
    {
        if (is_array($spec) && !is_callable($spec))
        {
            $spec = $this->makeClosure($spec);
        }
        return $this->parentMakeObserver($spec);
    }

    /**
     * Fire
     */
    public function fire ($tag, $args = [])
    {
        $e = $this->parentFire($tag, $args);

        if ($this->hasParent( )) {
            $this->getParent( )->fire($e);
        }
        return $e;
    }
}
