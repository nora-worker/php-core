<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Util\TagStorage;
use Nora\Core\Logging\Log;
use Nora\Core\Exception\ApplicationError;
use function Nora\__ as __;

/**
 * ヘルパマネージャー
 */
class HelperManager
{
    private $_scope;
    private $_helpers   = [];

    public function __construct (Scope $scope)
    {
        $this->_scope;
    }

    public function hasHelper($name)
    {
        $name = strtolower($name);

        if( isset($this->_helpers[$name]) || isset($this->_helpers[$name]))
        {
            return true;
        }
        return false;
    }

    public function getHelper($name)
    {
        $name = strtolower($name);

        if (isset($this->_helpers[$name]))
        {
            return $this->_helpers[$name];
        }

        throw new ApplicationError(
            sprintf(__('%sは定義されていません'), $name),
            $this
        );
    }

    public function setHelper($name, $spec)
    {
        $name = strtolower($name);
        $this->_helpers[$name] = $spec;
        return $this;
    }
}

