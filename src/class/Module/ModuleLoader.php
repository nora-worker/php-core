<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */

namespace Nora\Core\Module;

use Nora\Core\Component\Componentable;
use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use function Nora\__;

/**
 * モジュールローダクラス
 */
class ModuleLoader {

    use Componentable;

    const FACADE_CLASS="Facade";
    const LOADER_PATH="loader.php";
    const AUTO_LOADER_PATH="autoload.php";

    private $_ns_list = [];
    private $_path_list = [];

    protected function initComponentImpl( )
    {
        $this->getScope()->setComponent('ModuleLoader', $this);
    }


    public function hasModule($name)
    {
        foreach($this->Configure( )->read('module.ns') as $ns)
        {
            $class = $ns.'\\'.ucfirst($name).'\\'.self::FACADE_CLASS;
            if (class_exists($class))
            {
                return true;
            }
            return false;
        }
        return false;
    }

    public function load($name)
    {
        if (func_num_args() > 1)
        {
            $list = [];
            foreach(func_get_args() as $name)
            {
                $list[$name] = $this->load($name);
            }

            return $list;
        }

        if ($this->hasComponent($name))
        {
            return $this->getComponent($name);
        }

        foreach($this->Configure( )->read('module.ns') as $ns)
        {
            $class = $ns.'\\'.ucfirst($name).'\\'.self::FACADE_CLASS;
            if (class_exists($class))
            {
                $this->setComponent($name, $class::getFacadeSpec());
                /**
                $this->rootScope()->setComponent($name, function ( ) use ($name){
                    return $this->getComponent($name);
                });
                **/
                return $this->getComponent($name);

            }
        }


        $this->err(sprintf(__('%sはロードできませんでした'), $name));
        return false;
    }


}
