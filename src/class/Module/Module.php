<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */

namespace Nora\Core\Module;

use Nora\Core\Component\Componentable;
use Nora\Core\Util\Collection\Hash;

/**
 * モジュールクラス
 */
abstract class Module implements ModuleIF
{
    use Modulable;
}
