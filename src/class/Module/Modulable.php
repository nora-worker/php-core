<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */

namespace Nora\Core\Module;

use Nora\Core\Component\Componentable;

/**
 * モジュールクラス
 */
trait Modulable
{
    use Componentable;

    /**
     * Facade取得
     */
    static public function getFacadeSpec ( )
    {
        $class = get_called_class();
        return ['scope', function($scope) use ($class){
            $module =  new $class( );
            $module->setScope($scope->newScope(substr($class, strrpos($class,'\\'))));
            return $module;
        }];
    }

    protected function initComponentImpl( )
    {
        $this->initModule();
    }

    protected function initModule ( )
    {
        $this->initModuleImpl( );
    }

    abstract protected function initModuleImpl( );
}
