<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */

use Nora\Core\Autoloader;
use Nora\Core\Scope\Scope;
use Nora\Core\App\App;
use Nora\Core\Module\ModuleLoader;
use Nora\Core\Configuration\Configure;

/**
 * Noraのメインクラス
 *
 * 基本的な処理はself::$_applicationに保存された
 * オブジェクトに引き渡す
 */
class Nora 
{
    const LIB = __DIR__;
    const MODULE_PATH = __DIR__.'/../modules';

    /**
     * オートローダ
     */
    static private $_autoloader;

    /**
     * アプリ
     */
    static private $_app;

    /**
     * アプリクラス
     */
    static private $_app_class = 'Nora\Core\App\App';


    /**
     * オートローダの取得
     *
     * 初期化されていなければ、初期化する
     *
     * @return Autoloader
     * @codeCoverageIgnore
     */
    static public function Autoloader( ) 
    {
        if (self::$_autoloader) 
        {
            return self::$_autoloader;
        }

        require_once self::LIB.'/Autoloader.php';

        return 
            self::$_autoloader = 
                Autoloader::create(
                    [
                        'Nora\Core' => self::LIB,
                        'Nora\Module\Application' => self::MODULE_PATH.'/Application/class'
                    ]
                );
    }

    /**
     * プロジェクトの初期化
     */
    static public function initialize ($path, $env, $cb = null)
    {
        // 基本的な情報を設定
        Nora::Configure()->write('app', [
            'path'       => $path,
            'env'        => $env,
        ]);

        // FSモジュールの読み込み
        Nora::FileSystem_alias([
            '@config' => 'config',
            '@var'    => 'var',
            '@log'    => '@var/log',
            '@cache'  => '@var/cache'
        ]);

        // 設定ファイルディレクトリを追加
        Nora::Configure()->append('config.path', Nora::getFilePath('@config'));


        // 動的に設定を読み込む
        // config/${env}.php
        // config/${env}/${section}.php
        Nora::Configure()->load(
            Nora::Configure()->readArray('config.path'),
            'default',
            $env
        );

        Nora::module('environment', 'logging');

        // ハンドリング開始
        Nora::environment_register();

        // ロガーを立ち上げる
        Nora::logging_start();

        if ($cb !==  null)
        {
            Nora::inject($cb);
        }
    }

    static private function _setup()
    {
        self::$_app = new App( );
        self::$_app->setScope(new Scope('Nora'));
        self::$_app->setComponent([
            '@AutoLoader' => self::$_autoloader,
            '@Configure' => ['scope', function ($s) {
                $configure = new Configure();
                $configure->setScope($s->newScope('Configure'));

                $configure->write('module.ns', [
                    'Nora\Module'
                ]);
                $configure->write('config.path', [
                    realpath(__DIR__.'/../../config')
                ]);

                $configure->write('mbstring.language', 'Japanese');
                $configure->write('mbstring.internal_encoding', 'UTF-8');
                $configure->write('display_errors', 1);
                $configure->write('error_reporting', E_ALL);
                return $configure;
            }],
            '@ModuleLoader' => ['scope', function ($scope) {
                $loader = new ModuleLoader( );
                $scope->runScript(realpath(__DIR__.'/..').'/modules/autoload.php');
                $loader->setScope($scope->newScope('ModuleLoader'));
                return $loader;
            }]
        ]);
        self::$_app->setHelper([
            '@readConf' => ['Configure', function ($c, $key, $value = null) {
                return $c->read($key, $value);
            }],
        ]);

        Nora::ModuleLoader()->load('environment');
    }


    static public function module($name)
    {
        if (func_num_args() > 1) {
            $ret = [];
            for($i=0; $i<func_num_args(); $i++)
            {
                $ret[] = self::module(func_get_arg($i));
            }
            return $ret;
        }
        return self::ModuleLoader()->load($name);
    }


    /**
     * @param string $name 呼びだされたメソッド名
     * @param string $args 呼びだされたメソッドの引数
     * @return mixed
     */
    static public function __callStatic ($name, $args)
    {
        if (!self::$_app) {
            // 基本部分をセットアップ
            Nora::_setup();
        }
        return call_user_func_array(
            [self::$_app, $name],
            $args
        );
    }
}
