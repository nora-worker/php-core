<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Util;

class SpecLine
{
    private $_spec;
    private $_type = null;
    private $_subType = null;
    private $_path = null;
    private $_attrs = null;
    static public function create($spec)
    {
        if ($spec instanceof SpecLine) return $spec;
        return new self($spec);
    }

    public function __construct($spec)
    {
        $this->_spec = $spec;
    }

    private function parseType( )
    {
        $type = $sub = false;

        if (false !== $p = strpos($this->_spec, '://'))
        {
            $type = substr($this->_spec, 0, $p);
            $lest = substr($this->_spec, $p+3);
            $data = explode(";", $lest, 2);
            $this->_path = $data[0];
            if (!empty($data[1]))
            {
                parse_str($data[1], $this->_attrs);
            }
        }else{
            $type = $this->_spec;
            $this->_path = null;
        }

        if (false !== $p = strpos($type, '+'))
        {
            $sub = substr($type, $p+1);
            $type = substr($type, 0, $p);
        }

        $this->_type = $type;
        $this->_subType = $sub;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function setSubType($type)
    {
        $this->_subType = $type;
    }

    public function getType( )
    {
        if ($this->_type === null) {
            $this->parseType();
        }

        return $this->_type;
    }

    public function getPath( )
    {
        if ($this->_path === null) {
            $this->parseType();
        }

        return $this->_path;
    }

    public function getSubType( )
    {
        if ($this->_subType === null) {
            $this->parseType();
        }

        return $this->_subType;
    }

    public function getAttrs( )
    {
        return $this->_attrs;
    }

    public function __toString()
    {
        $type = $this->getType();
        $path = $this->getPath();
        $attars = http_build_query($this->getAttrs());
        $string = $type;
        $string.= '://'.$path;
        $string.= ';'.$attars;
        return $string;
    }
}
