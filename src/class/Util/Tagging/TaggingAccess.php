<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Util\Tagging;


trait TaggingAccess
{
    private $_tag;

    /**
     * タグを追加する
     */
    public function addTag($tag)
    {
        $this->getTagStorage()->add($tag);
        return $this;
    }

    /**
     * タグを表示する
     */
    public function printTag( )
    {
        foreach($this->getTagStorage() as $tag)
        {
            echo $tag."\n";
        }
    }

    /**
     * タグストレージを取得する
     */
    public function getTagStorage( )
    {
        if ($this->_tag instanceof TagStorage)
        {
            return $this->_tag;
        }
        $this->_tag = new TagStorage();
        return $this->getTagStorage();
    
    }

    /**
     * タグ検索
     */
    public function searchTag($neadle, &$hit)
    {
        if (is_string($neadle)) {
            return $this->searchTag(
                function($tag) use ($neadle){
                    return 0 === strncasecmp($tag, $neadle, strlen($neadle));
                },
                $hit
            );
        }

        if ($neadle instanceof \Closure)
        {
            foreach($this->getTagStorage() as $tag)
            {
                if(call_user_func($neadle, $tag))
                {
                    $hit = $tag;
                    return true;
                }
            }
            return false;
        }
    }
}
