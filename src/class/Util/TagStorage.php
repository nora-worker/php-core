<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Util;

/**
 * タグ用ストレージ
 */
class TagStorage
{
    private $_array = [];

    /**
     * ストレージを作成
     */
    static public function create ($array)
    {
        $ts = new TagStorage();
        $ts->add($array);
        return $ts;
    }

    /**
     * タグを追加する
     *
     * @param string $tag
     */
    public function add($tag)
    {
        if ($tag === null) return $this;

        if ($tag instanceof TagStorage)
        {
            var_dump($tag->_array);
            //return $this->add($tag->_array);
        }

        if (is_array($tag)) {
            foreach($tag as $v) $this->add($tag);
            return $this;
        }

        if ($offset = array_search($tag, $this->_array))
        {
            unset($this->_array[$offset]);
        }

        array_unshift($this->_array, $tag);
        return $this;
    }

    /**
     * 文字化する
     */
    public function toString($sep = ', ')
    {
        return implode($sep, $this->_array);
    }

    /**
     * 配列として出力
     */
    public function toArray($extra = [])
    {
        $tag = clone $this;
        $tag->add($extra);
        return $tag->_array;
    }
}

