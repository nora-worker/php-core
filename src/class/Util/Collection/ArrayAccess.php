<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Util\Collection;

/**
 * 配列アクセス
 *
 * \ArrayAccessの実装
 */
trait ArrayAccess
{
    /**
     * データ・セット
     *
     * 第一引数に配列を与えれば
     * 複数件を一括でセットできる
     *
     * @param string $name
     * @param mixed $value
     */
    abstract public function set ($name, $value = null);

    /**
     * データの取得
     *
     * 参照で返す。
     * 第二引数をセットすると値が取得できなかった時の
     * 初期値として使える
     *
     * @param string $name
     * @param mixed $default
     */
    abstract function &get ($name, $default = null);

    /**
     * データの削除
     *
     * @param string $name
     */
    abstract public function clear($name);

    /**
     * データの存在チェック
     *
     * データが設定されていれば、
     * NullでもEmptyでもTrue
     *
     * @param string $name
     * @param mixed $value
     */
    abstract public function has ($name);


    public function offsetExists($name)
    {
        return $this->has($name);
    }

    public function &offsetGet($name)
    {
        return $this->get($name);
    }

    public function offsetSet($name, $value)
    {
        return $this->set($name, $value);
    }

    public function offsetUnset($name)
    {
        return $this->clear($name);
    }
}

