<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Util\Collection;

/**
 * オブジェクトハッシュ
 */
class ObjectHash implements \ArrayAccess,\IteratorAggregate
{
    use ArrayAccess;

    private $_array = [];
    private $_data;
    private $_flg = 0;


    public function add($object)
    {
        $this->_array[spl_object_hash($object)] = $object;
    }

    public function remove($object)
    {
        unset($this->_array[spl_object_hash($object)]);
    }


    public function set($object, $vars)
    {
        if (!$this->has($object))
        {
            $this->add($object);
        }
        $this->_data[spl_object_hash($object)] = $vars;
    }

    public function has ($object)
    {
        return isset($this->_array[spl_object_hash($object)]);
    }

    public function &get ($object, $default = null)
    {
        if ($this->has($object))
        {
            return $this->_data[spl_object_hash($object)];
        }
        return $default;
    }

    public function clear($object)
    {
        $this->remove($object);
        unset( $this->_data[spl_object_hash($object)]);
    }

    public function getIterator() {
        foreach($this->_array as $k=>$v)
        {
            yield $v;
        }
    }

}
