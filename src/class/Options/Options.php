<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Options;

use Nora\Core\Exception\ApplicationError;
use function Nora\__;


/**
 * 設定構成
 */
class Options
{
    private $_array = [];

    public function init ($setting)
    {
        foreach($setting as $k=>$v)
        {
            $this->set($k, $v, true);
        }
    }

    public function has ($name)
    {
        return isset($this->_array[$name]);
    }

    public function get ($name, $value=null, $trace = null)
    {
        if ($trace === null) $trace = $name;

        $data = $this->_array;
        while(false !== $p = strpos($name, '.'))
        {
            $cursor = substr($name, 0, $p);
            $name = substr($name, $p + 1);
            $data = $data[$cursor];
        }

        if (array_key_exists($name, $data))
        {
            return $data[$name];
        }

        throw new ApplicationError(
            sprintf(__('%sは存在しないオプションです'), $trace),
            $this
        );
    }

    public function set ($name, $value, $allow_new = false, $trace = null)
    {
        if ($trace === null) $trace = $name;

        if (array_key_exists($name, $this->_array))
        {
            $this->_array[$name] = $value;
            return $this;
        }

        if ($allow_new === true)
        {
            $this->_array[$name] = $value;
            return $this;
        }

        vaR_dump(array_keys($this->_array));
        throw new ApplicationError(
            sprintf(__('%sは存在しないオプションです'), $trace),
            $this
        );
    }

    public function toArray( ) {
        $ret =[];
        foreach($this->_array as $k=>$v) {
            if ($v instanceof Options) {
                $ret[$k] = $v->toArray();
            } else{
                $ret[$k] = $v;
            }
        }
        return $ret;
    }

}
