<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Options;


/**
 * 設定構成
 */
trait OptionsAccess
{
    private $_options = false;

    /**
     * Storage
     */
    protected function options ( ) {
        if ($this->_options === false)
        {
            $this->_options = new Options( );
        }
        return $this->_options;
    }

    /**
     * 初期化
     */
    public function initOptions($defaults) {
        $this->options()->init($defaults);
    }

    /**
     * 設定する
     */
    public function setOption($name, $value = null) {
        if (is_array($name))
        {
            foreach($name as $k=>$v) $this->options()->set($k, $v);
        }
        return $this;
    }

    /**
     * 取得する
     */
    public function getOption($name, $value = null) {
        return $this->_options->get($name, $value);
    }

    /**
     * 取得する
     */
    public function getOptions( ) {
        return $this->_options->toArray();
    }

    public function hasOption($name) {
        return $this->_options->has($name);
    }
}
