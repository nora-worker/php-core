<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\App;

use Nora\Core\Component\Component;

/**
 * アプリケーションクラス
 */
class App extends Component
{
    /**
     * 名前
     */
    private $_name;

    /**
     * 初期化
     */
    public function __construct($name = 'App')
    {
        $this->_name = $name;
    }

}
