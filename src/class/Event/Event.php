<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Event;

use Nora\Core\Exception;

use Nora\Core\Util\Collection;

use Nora\Core\Util\Tagging\TaggingAccess;
use Closure;

/**
 * イベント
 */
class Event implements EventIF
{
    /**
     * タグ付けを可能にする
     */
    use TaggingAccess;

    private $_propagation = true;
    private $_params = [];
    private $_subject = null;

    /**
     * コンストラクタ
     *
     * @param array $params
     * @param mixed $subject
     */
    public function __construct ( )
    {
    }

    /**
     * イベントの作成
     *
     * @param array|string $tag
     * @param array $params
     * @param object|null $subject
     */
    static public function create ($tag, $params = [], $subject = null)
    {
        $e = new Event( );
        $e->_params = $params;
        $e->addTag($tag);
        $e->setSubject($subject);
        return $e;
    }

    /**
     * タグとマッチするか
     */
    public function match($tag, &$hit = null)
    {
        return $this->searchTag($tag, $hit);
    }

    /**
     * サブジェクトをセット
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;
    }

    /**
     * サブジェクトの有無
     */
    public function hasSubject( )
    {
        return isset($this->_subject) && !is_null($this->_subject);
    }

    /**
     * サブジェクトを取得する
     */
    public function getSubject( )
    {
        return $this->_subject;
    }

    /**
     * パラム名を取得
     */
    public function getParamNames( )
    {
        return array_keys($this->_params);
    }

    /**
     * パラムを取得
     */
    public function getParams( )
    {
        return $this->_params;
    }


    /**
     * パラメタ操作
     */
    public function &__get($name)
    {
        if (!array_key_exists($name,$this->_params))
        {
            throw new Exception\InvalidProperty($name, $this);
        }
        return $this->_params[$name];
    }

    /**
     * パラメタ操作
     */
    public function __set($name, $value)
    {
        if (!array_key_exists($name,$this->_params))
        {
            throw new Exception\InvalidProperty($name, $this);
        }
        return $this->_params[$name] = $value;
    }

    /**
     * パラメタ操作
     */
    public function __isset($name)
    {
        return array_key_exists($name,$this->_params);
    }

    /**
     * 伝搬
     */
    public function stopPropagation ( )
    {
        $this->_propagation =  false;
    }

    /**
     * 伝搬が止まっているか
     */
    public function isStopPropagation ( )
    {
        return !$this->_propagation;
    }
}
