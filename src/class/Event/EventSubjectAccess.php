<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Core\Event;

use Nora\Core\Util\Collection;

/**
 * イベントサブジェクト
 */
trait EventSubjectAccess
{
    private $_observers = false;

    /**
     * Observe
     */
    public function observe ($spec)
    {
        $this->observers()->add(
            $obs = $this->makeObserver($spec)
        );
        return $obs;
    }

    /**
     * UnObserve
     */
    public function detach ($observer)
    {
        $this->observers()->clear($observer);
        return $observer;
    }

    /**
     * Fire
     */
    public function fire ($tag, $args = [])
    {
        if ($tag instanceof Event) {
            $event = $tag;
        }else{
            $event = Event::create($tag, $args, $this);
        }

        if ($event->isStopPropagation()) return $event;

        foreach($this->observers() as $o)
        {
            if(false === $o->notify($event))
            {
                $event->stopPropagation();
                break;
            }
        }

        return $event;
    }

    /**
     * オブザーバリストを取得する
     */
    protected function observers( )
    {
        if ($this->_observers === false)
        {
            $this->_observers = new Collection\ObjectHash( );
        }
        return $this->_observers;
    }

    /**
     * オブザーバを作る
     */
    protected function makeObserver($spec)
    {
        return Observer::create($spec);
    }
}
