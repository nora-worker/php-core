<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Event\Observer;

use Nora\Core\Event\Event;

/**
 * イベントのコールバック
 */
class Closure
{
    private $_cb;

    public function __construct($cb)
    {
        $this->_cb = $cb;
    }

    public function notify (Event $e)
    {
        return call_user_func($this->_cb, $e);
    }
}

