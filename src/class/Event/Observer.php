<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Event;

use Closure;

/**
 * イベント
 */
class Observer
{
    /**
     * イベントオブザーバの作成
     *
     * @param mixed $spec
     */
    static public function create ($spec)
    {
        $observer = new self();
        $observer->setType(new Observer\Closure($spec));
        return $observer;
    }

    private function setType(Observer\Closure $type)
    {
        $this->_type = $type;
    }

    public function notify (Event $e)
    {
        return $this->_type->notify($e);
    }
}
