<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Exception;

/**
 * アプリケーションエラー
 */
class ApplicationError extends Exception
{
    public function __construct ($msg, $level = 'error')
    {
        parent::__construct($msg);
    }
}
