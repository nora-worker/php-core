<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Devel;

use Nora\Core\Component\Component;
use Nora\Core\Util\Collection\Hash;
use Nora\Core\Scope\ScopeIF;
use Nora\Core\Module\ModuleIF;
use Nora\Core\Module\Modulable;

/**
 * DEVELOPモジュール
 */
class Facade implements ModuleIF
{
    use Modulable;

    protected function initModuleImpl( )
    {
        $this->enable($this->Configure_read('devel.enable'));
    }

    public function enable($bool)
    {
        $this->rootScope()->setHelper([
            '@d' => function ($var) use ($bool) {
                if ($bool === true) var_dump($var);
            }
        ]); 
    }
}
