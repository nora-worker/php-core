<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Logging;

use Nora\Core\Component\Componentable;
use Nora\Core\Module\ModuleIF;
use Nora\Core\Module\Modulable;

/**
 * ロガーファサード
 */
class Facade extends Logger implements ModuleIF
{
    use Modulable;

    protected function start( )
    {
        $this->attach($this->rootScope());

        foreach($this->Configure_read('logging') as $setting)
        {
            $this->addLogger($setting->toArray());
        }
    }

}

