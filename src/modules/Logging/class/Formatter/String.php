<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging\Formatter;

use Nora\Core\Logging\Log;

/**
 *
 */
class String
{
    private $_order = ['@level', '@fields', '@time', '@tag'];

    public function __construct($spec)
    {
        $this->_format = preg_replace_callback('/%{([^}]+)}/', function ($m) {
            if (false === $pos = array_search($m[1], $this->_order))
            {
                return $m[0];
            }
            return '%'.($pos+1).'$s';
        }, ltrim($spec, '/'));
    }

    public function format (Log $log)
    {
        $array = [
            $log->getLevelText( ),
            $log->getFieldsText( ),
            $log->getTime(),
            $log->getTagText( )
        ];

        return vsprintf($this->_format, $array);
    }
}

