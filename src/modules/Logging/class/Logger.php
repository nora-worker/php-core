<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging;

use Nora\Core\Scope\Scope;
use Nora\Core\Options\OptionsAccess;
use Nora\Core;

/**
 * ロガー
 */
class Logger implements LoggerIF
{
    use OptionsAccess;

    private $_loggers = [];
    private $_writer = false;
    private $_detacher = false;

    public function __destruct()
    {
        $this->detach();
    }

    public function addLogger($logger)
    {
        if (!($logger instanceof LoggerIF))
        {
            $logger = $this->create($logger);
        }
        $this->_loggers[] = $logger;
        return $this;
    }

    public function detach( )
    {
        if ($this->_detacher !== false)
        {
            call_user_func($this->_detacher);
        }
        return $this;
    }


    /**
     * スコープにアタッチする
     */
    public function attach (Scope $scope)
    {
        $observer = $scope->observe(function ($e) {
            if ($e->match('php.error'))
            {
                $this->write(
                    Log::create(
                        LogLevel::phpToNora($e->errno), [
                            'message' => $e->errstr,
                            'file' => $e->errfile,
                            'line' => $e->errline,
                            'php_error_coce' => $e->errno
                        ], 'php.error'
                    )
                );
            }elseif($e->match('php.exception')) {
                $this->write(
                    Log::create(
                        LogLevel::CRIT, [
                            'message' => (string) $e->exception,
                            'class' => get_class($e->exception),
                        ], 'php.exception'
                    )
                );
            }elseif($e->match('log')) {
                $this->write($e->log);
            }
        });

        $this->_detacher = function ( ) use ($scope, $observer) {
            $scope->detach($observer);
        };

        return $this;
    }

    /**
     * ロガーを作成する
     */
    static public function create ($options = [])
    {
        $logger = new Logger();
        $logger->setOption($options);
        $logger->setup();
        return $logger;
    }

    /**
     * ロガーをセットアップする
     */
    public function setup( )
    {
        $writer = $this->createWriter($this->getOption('writer'));

        // フォーマットを設定
        $writer->setFormatter(
            $this->createFormatter($this->getOption('format')));

        // フィルターを設定
        foreach($this->getOption('filter', []) as $spec)
        {
            $writer->addFilter(
                $this->createFilter($spec));
        }

        $this->_writer = $writer;
    }

    public function createWriter($spec)
    {
        if (false !== $p = strpos($spec, ':'))
        {
            $type = substr($spec, 0, $p);
            $spec = substr($spec, $p+3);
        }else{
            $type = $spec;
            $pec = null;
        }

        // ライターをセットアップ
        $class = sprintf(__NAMESPACE__.'\\Writer\\%s', ucfirst($type));
        $writer = new $class($spec);
        return $writer;
    }

    public function createFormatter($spec)
    {
        $type = strtok($spec, ':');

        // フォーマッタをセットアップ
        $class = sprintf(__NAMESPACE__.'\\Formatter\\%s', ucfirst($type));
        $formatter = new $class(strtok(''));
        return $formatter;
    }

    public function createFilter($spec)
    {
        if (false !== $p = strpos($spec, ':'))
        {
            $type = substr($spec, 0, $p);
            $spec = substr($spec, $p+3);
        }

        // フィルターをセットアップ
        $class = sprintf(__NAMESPACE__.'\\Filter\\%s', ucfirst($type));
        $filter = new $class($spec);
        return $filter;
    }
    public function __construct( )
    {
        // 許容するオプションを指定
        $this->initOptions([
            'format' => 'string://%{@time} %{@level} %{@fields} [%{@tag}] ',
            'writer' => 'stdout',
            'filter' => [ ]
        ]);
    }


    /**
     * ログを書き込む
     */
    public function write(Core\Logging\Log $log)
    {
        if ($this->_writer) $this->_writer->write($log);

        // 追加されたロガーへ処理を渡す
        foreach ($this->_loggers as $logger)
        {
            $logger->write($log);
        }
    }

}
