<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Logging;

use Nora\Core\Scope\Scope;

/**
 * ロガーIF
 */
interface LoggerIF
{
    /**
     * スコープにアタッチする
     */
    public function attach (Scope $scope);
}

