<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Logging\Filter;

use Nora\Core\Logging\Log;
use Nora\Core\Logging\LogLevel;

/**
 *
 */
class Level
{
    private $_level;

    public function __construct($spec)
    {
        $this->_level = LogLevel::toInt($spec);
    }

    public function filter (Log $log)
    {
        return $log->getLevel() <= $this->_level ? true: false;
    }
}

