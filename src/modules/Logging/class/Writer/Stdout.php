<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging\Writer;

use Nora\Core\Logging\Log;

/**
 *
 */
class Stdout extends Base
{
    protected function writeImpl(Log $log)
    {
        echo
            $this->getFormatter( )->format($log)."\n";
    }
}

