<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging\Writer;

use Nora\Core\Logging\Log;

/**
 * ログライター For File
 */
class File extends Base
{
    /**
     * ログファイル
     */
    private $_file;

    /**
     * 書き込みモード
     */
    private $_mode;


    /**
     * 初期処理
     */
    protected function initWriterImpl ( )
    {
        $file = preg_replace_callback('/%\{(.+)\}/U', function ($m) {
            $num = array_search($m[1], $this->_format_order);

            if (false !== $p = strpos($m[1], ':'))
            {
                $key = substr($m[1], 0, $p);
                $param = substr($m[1], $p+1);
            }
            else{
                $key = $m[1];
                $param = null;
            }

            switch($key)
            {
                case 'user':
                    return posix_getpwuid(posix_getuid())['name'];
                case 'date':
                    return date($param);
                default:
                    return $m[1];
            }
        }, strtok($this->spec(), ';'));


        while($data = strtok(';'))
        {
            parse_str($data, $params);
        }


        if (!file_exists($file))
        {
            if (!is_dir(dirname($file)))
            {
                mkdir(dirname($file), 0777, true);
                chmod(dirname($file), 0777);
            }
            touch($file);
            chmod($file, 0666);
        }

        $this->_file = $file;
        $this->_mode = isset($params['mode']) ? $params['mode']: 'a';
    }

    public function writeImpl(Log $log)
    {
        $line = $this->getFormatter( )->format($log);

        $fp = fopen($this->_file, $this->_mode);
        flock($fp, LOCK_EX);
        fwrite($fp, $line."\n");
        flock($fp, LOCK_UN);
        fclose($fp);
    }
}
