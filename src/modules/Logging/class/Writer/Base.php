<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging\Writer;

use Nora\Core\Logging\Log;
use Nora\Core\Logging\LogLevel;

/**
 * ログライターの基礎
 */
abstract class Base
{
    private $_spec;
    private $_formatter;
    private $_filters =[];

    public function __construct($spec)
    {
        $this->_spec = $spec;

        $this->initWriter( );
    }

    protected function initWriter( )
    {
        $this->initWriterImpl();
    }

    protected function initWriterImpl( )
    {
    }

    protected function spec( )
    {
        return $this->_spec;
    }

    public function addFilter($filter)
    {
        $this->_filters[] = $filter;
        return $this;
    }

    public function setFormatter($formatter)
    {
        $this->_formatter = $formatter;
        return $this;
    }

    public function getFormatter( )
    {
        if ($this->_formatter)
            return $this->_formatter;
    }

    public function write(Log $log)
    {
        foreach($this->_filters as $filter)
        {
            if(!$filter->filter($log))
            {
                return false;
            }
        }
        $this->writeImpl($log);
    }

    abstract protected function writeImpl(Log $log);
}

