<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Logging;

use Nora\Core\Exception\ApplicationError;

/**
 * ログの機能
 */
trait LoggerAccess
{
    /**
     * Debugレベルのログ
     */
    public function logDebug($msg, $tags = null)
    {
        $this->postLog('Debug', $msg, $tags);
    }

    /**
     * Noticeレベルのログ
     */
    public function logNotice($msg, $tags = null)
    {
        $this->postLog('Notice', $msg, $tags);
    }

    /**
     * Infoレベルのログ
     */
    public function logInfo($msg, $tags = null)
    {
        $this->postLog('Info', $msg, $tags);
    }

    /**
     * Warningレベルのログ
     */
    public function logWarning($msg, $tags = null)
    {
        $this->postLog('Warning', $msg, $tags);
    }

    /**
     * Errレベルのログ
     */
    public function logErr($msg, $tags = null)
    {
        $this->postLog('err', $msg, $tags);
    }

    /**
     * Critレベルのログ
     */
    public function logCrit($msg, $tags = null)
    {
        $this->postLog('Crit', $msg, $tags);
    }

    /**
     * Alertレベルのログ
     */
    public function logAlert($msg, $tags = null)
    {
        $this->postLog('alert', $msg, $tags);
    }

    /**
     * Emergeレベルのログ
     */
    public function logEmerge($msg, $tags = null)
    {
        $this->postLog('emerge', $msg, $tags);
    }

    /**
     * エラーを起こす
     */
    public function err($msg)
    {
        $this->logErr($msg);
        $this->raizeError($msg);
    }

    /**
     * クリティカルエラーを起こす
     */
    public function crit($msg)
    {
        $this->logCrit($msg);
        $this->raizeError($msg);
    }

    /**
     * アラートを起こす
     */
    public function alert($msg)
    {
        $this->logAlert($msg);
        $this->raizeError($msg);
    }

    /**
     * エマージェンシーエラーを起こす
     */
    public function emerg($msg)
    {
        $this->logEmerg($msg);
        $this->raizeError($msg);
    }

    /**
     * エラーを起こす
     */
    public function raizeError($msg)
    {
        throw new ApplicationError($msg, $this);
    }

    /**
     * ログ受信時の処理
     */
    protected function postLog ($level, $msg, $tags = [])
    {
        // ログオブジェクトの作成
        $log = Log::create(
            $level,
            $msg,
            $tags
        );

        // ログを投げる
        $this->log($log);
    }
}
