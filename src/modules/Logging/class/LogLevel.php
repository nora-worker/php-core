<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging;

use Nora\Core;

/**
 * ログレベルを保持したり、変換したりするクラス
 */
class LogLevel extends Core\Logging\LogLevel
{
}
