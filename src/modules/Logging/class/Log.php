<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.org>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.org/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Logging;

use Nora\Core\Util\Tagging\TaggingAccess;

use Nora\Core;


/**
 * ログオブジェクト
 */
class Log extends Core\Logging\Log
{
}
