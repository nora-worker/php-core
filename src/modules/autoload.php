<?php
/**
 * モジュール用のローダ
 */
return ['Autoloader', 'Configure', function ($al,$c) {

    $c->append('module.ns', 'Nora\Module');

    $al->addLibrary([
        'Nora\Module\FileSystem'  => __DIR__.'/FileSystem/class',
        'Nora\Module\Devel'       => __DIR__.'/Devel/class',
        'Nora\Module\Environment' => __DIR__.'/Environment/class',
        'Nora\Module\Logging' => __DIR__.'/Logging/class',
    ]);
}];

