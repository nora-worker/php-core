<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\Environment;

use Nora\Core\Component\Componentable;
use Nora\Core\Util\Collection\Hash;

/**
 * Environmentクラス
 */
class Environment 
{
    use Componentable;

    protected function initComponentImpl( )
    {
        $this->rootScope()->setHelper(
            [
                '@getEnv' => [$this,'getEnv']
            ]
        );

        $this->setComponent(
            [
                'ENV' => function ( ) {
                    return Hash::create(Hash::NO_CASE_SENSITIVE, $_ENV);
                },
                'SERVER' => function ( ) {
                    return Hash::create(Hash::NO_CASE_SENSITIVE, $_SERVER);
                },
                'GET' => function ( ) {
                    return Hash::create(Hash::NO_CASE_SENSITIVE, $_GET);
                },
                'POST' => function ( ) {
                    return Hash::create(Hash::NO_CASE_SENSITIVE, $_POST);
                },
                'COOKIE' => function ( ) {
                    return Hash::create(Hash::NO_CASE_SENSITIVE, $_COOKIE);
                },
            ]
        );
    }

    public function bootPHP( )
    {
        return new PHPFunctionWrapper( );
    }

    public function bootDetector( )
    {
        $detector = new Detector($this);
        return $detector;
    }

    public function is ($name)
    {
        return $this->Detector()->is($name);
    }

    public function getEnv($name, $default = null)
    {
        $name = strtolower($name);
        if (isset($this->_env[$name])) return $this->_env[$name];
        if ($this->ENV()->has($name)) return $this->ENV()->get($name);
        if ($this->SERVER()->has($name)) return $this->SERVER()->get($name);

        return $default;
    }

    public function setEnv($name, $value = null)
    {
        if (is_array($name)) {
            foreach($name as $k=>$v) $this->setEnv($k, $v);
            return $this;
        }
        $nme = strtolowe($name);
        $this->_env[$name] = $value;
        return $this;
    }

    public function register ( )
    {
        // ハンドラ系
        set_error_handler([$this, 'phpErrorHandler']);
        set_exception_handler([$this, 'phpExceptionHandler']);
        register_shutdown_function([$this, 'phpShutdownHandler']);
        return $this;
    }

    /**
     *  int $errno , string $errstr [, string $errfile [, int $errline [, array $errcontext ]
     */
    public function phpErrorHandler($errno, $errstr, $errfile, $errline, $errcontext)
    {
        $this->rootScope()->fire('php.error', [
            'errno'      => $errno,
            'errstr'     => $errstr,
            'errfile'    => $errfile,
            'errline'    => $errline,
            'errcontext' => $errcontext
        ]);
    }

    /**
     */
    public function phpExceptionHandler($exception)
    {
        $this->rootScope()->fire('php.exception', [
            'exception' => $exception
        ]);
    }

    /**
     */
    public function phpShutdownHandler( )
    {
        $error = error_get_last();
        if ($error['type'] > 0) {
            $this->phpErrorHandler(
                $error['type'],
                $error['message'],
                $error['file'],
                $error['line'],
                []
            );
        }
        $this->fire('php.shutdown');
    }
}
