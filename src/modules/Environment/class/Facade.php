<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Environment;

use Nora\Core\Module\Modulable;
use Nora\Core\Module\ModuleIF;

/**
 * Environment モジュール
 */
class Facade extends Environment implements ModuleIF
{
    use Modulable;

    protected function initModuleImpl( )
    {
        parent::initComponentImpl();
    }
}
