<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\FileSystem;

use Nora\Core\Module\ModuleIF;
use Nora\Core\Module\Modulable;

/**
 * ファイルシステムモジュール
 */
class Facade implements ModuleIF
{
    use Modulable;

    private $_filesystem;


    public function initModuleImpl( )
    {
        $this->_filesystem = new FileSystem($this->Configure()->read('app.path'));

        $this->rootScope()->setHelper('@getFilePath', ['scope', function ($s, $file = null) {
            return $this->_filesystem->getPath($file);
        }]);
    }

    /**
     * ファイルシステムを作成する
     */
    public function newFileSystem($path)
    {
        return new FileSystem($path);
    }

    public function getPath($file = null)
    {
        return $this->_filesystem->getPath($file);
    }

    public function alias( )
    {
        return call_user_func_array(
            [$this->_filesystem, 'alias'],
            func_get_args()
        );
    }
}
