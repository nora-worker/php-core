<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora;

use Nora;

require_once realpath(__dir__.'/../').'/class/Nora.php';

// オートローダを登録する
Nora::Autoloader( )->register();

if (!function_exists('Nora\__'))
{
    function __($msg) {
        if (false && $msg === '%sは存在しません') {
            $msg = '%s is not exists';
        }
        return $msg;
    }
}
