<?php
// デフォルト
return [
    'name'                       => 'Nora',
    'mbstring.language'          => 'Japanese',
    'mbstring.internal_encoding' => 'UTF-8',
    'error_reporting'            => 0,
    'display_errors'             => 0,
];
