<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora;

use Nora\Core\Scope\Scope;
use Nora\Core\Configuration\Configure;
use function Nora\__;

use Nora;

class NoraTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate ( )
    {
        Nora::module('logging');

        Nora::module('environment')->logger();
        Nora::module('environment')->logInfo('aaa');


        Nora::initialize(TEST_PROJECT_PATH, 'dev', ['self', function ($self)  {
            var_Dump($self->getName());
        }]);

        Nora::configure_write('devel.enable', false);
        Nora::moduleLoader_load('devel');
        trigger_error('hoge');

        Nora::environment_env_set('HTTP_HOST', 'localhost');
        Nora::environment_env_set('REQUEST_METHOD', 'POST');

        var_Dump(Nora::environment_env_get('HTTP_HOST'));
        var_Dump(Nora::environment_is('get'));

        // ファイルモジュール
        var_dump(
            Nora::getFIlePath('@log')
        );



        // コンポーネントの差し込み
        Nora::setComponent('a', ['scope', function ($s) {
            return $s->newScope()->setHelper('test', function ( ) {
                return 'hoge';
            });
        }]);
    }
}

