<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\App;

use Nora\Core\Scope\Scope;
use Nora\Core\Configuration\Configure;
use Nora\Core\Environment\Environment;
use Nora\Core\Logging\Logger;
use function Nora\__;

use Nora;

class TestModule {

    public function status( ) {
        return [
            'a' => 'b'
        ];
    }
}

class AppTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate ( )
    {
        // スコープを作成する
        $scope = new Scope('test');


        $child = $scope->newScope('child');

        $scope->setComponent([
            'Hoge' => new TestModule(),
            'Fuga' => $scope,
            'child' => $child
        ]);


        $scope->setHelper([
            'help' => ['scope','self', function ($s, $self, $str) {
                var_dump($s->getName().' help = '. $str.' '.get_class($self));
            }]
        ]);


        // コンフィグを作成する
        $config = new Configure( );
        $config->write('debug', true);
        $config->write('mbstring.language', 'Japanese');
        $config->write('mbstring.internal_encoding', 'UTF-8');
        $config->write('display_errors', 1);
        $config->write('error_reporting', E_ALL);
        $config->write('logging', ['default' => ['type' => 'stdout']]);
        $config->write('logging.default.level', 'debug');


        // Scopeへコンフィグを組み込む
        $scope->setComponent('Configure', $config);

        // 環境を作成する
        $env = new Environment( );
        $env->setScope($scope->newScope('Environment'));
        $env->setConfigure($config);
        $env->register();

        // ロガーをアタッチする
        $logger = Logger::create([
            'filter' => [
                'tag://nora'      # タグがnora*
            ]
        ])
        ->addLogger(
            # ロガーを追加
            Logger::create([
                'filter' => [
                    'level://debug', # レベル debug以上
                    'tag://php'      # タグがphp*
                ]
            ])
        )
        ->attach($scope);

            


        $env->logDebug('ほげ', 'hoho');
        $env->logWarning('ほげ', 'hoho', 'nora');
        $env->logWarning('のら用');


        // APPを作成する
        $app = new App();
        $app->setScope( $scope->newScope('app') );
        $app->setComponent('env', $env);

        $app->setHelper('a', ['self', function ($self) {
            $self->logDebug('ほげ');
        }]);

        // InjectionでSelfが使える
        $app->inject(['self', function ($self) {
            //var_dump($self->status());
        }]);

        // コンポーネントを透過的に呼び出す
        $this->assertEquals(
            $app->hoge_status(),
            $app->fuga_hoge_status()
        );

        $app->call('help', 'a');
    }
}
