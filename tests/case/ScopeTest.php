<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Core\Scope;

use Nora\Core\Scope\Scope;
use Nora\Core\Exception\ApplicationError;
use function Nora\__;

use Nora;

class ScopeTest extends \PHPUnit_Framework_TestCase
{
    public function testEvent ( )
    {
        // スコープを作成する
        $scope = new Scope();

        // イベントの監視
        $scope->observe(function($e) {

            if($e->match('log', $hit)) {
                $this->assertEquals('log.hogehoge', $hit);
            }

            if($e->match('child', $hit)) {
                $this->assertEquals('child.event', $hit);
                $e->name = "OK";
            }
        });

        // イベントの実行
        $scope->fire('log.hogehoge', [
            'log' => "aaaa"
        ]);

        // 子供から親へ
        // Event->name = NG => OKに変わる
        $e = $scope->newScope()->fire('child.event', [
            'name' => 'NG'
        ]);

        $this->assertEquals('OK', $e->name);

        // Event->name = NG => OKに変わる手前でイベントをストップ
        $e = $scope->newScope()->observe(function($e) {
            $e->name = 'OOOOK';
            return false;
        })->fire('child.event', [
            'name' => 'NG'
        ]);

        $this->assertEquals('OOOOK', $e->name);
    }

    public function testComponent ( )
    {
        // スコープを作成する
        $scope = new Scope();

        $scope->setComponent(
            [
                'hoge' => function ( ) {
                    return new \StdClass();
                }
            ]
        );

        $scope->hoge();


        try
        {
            $scope->newScope()->hoge();
        }catch(ApplicationError $e){
            // 親コンポーネントは呼び出せない
            $this->assertTrue(true);
        }

        // Inject経由で呼び出す
        $scope->newScope('child1')->newScope()->inject([
            'scope',
            'hoge',
            function ($scope, $hoge) {
                var_Dump($scope->getName());
                var_Dump($hoge);
            }
        ]);



    }
}
