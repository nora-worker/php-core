<?php

return [
    [
        'writer' => 'file://'.$this->getFilePath('@log/dev-info-%{user}-%{date:Y-m-d};mode=a'),
        'filter' => [
            'level://debug'
        ]
    ],
    [
        'writer' => 'file://'.$this->getFilePath('@log/dev-error-%{user}-%{date:Y-m-d};mode=a'),
        'filter' => [
            'level://err'
        ]
    ],
    [
        'writer' => 'stdout'
    ]
];
